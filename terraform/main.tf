provider "google" {
  credentials = file("/home/smitha-joshi/Downloads/my-task-project-401014-a4c28db869e6.json")
  project     = "my-task-project-401014"
  region      = "us-south1"
}

resource "google_container_cluster" "my_test_cluster" {
  name               = "my-test-cluster"
  location           = "us-south1"
  initial_node_count = 1

   master_auth {
    client_certificate_config {
      issue_client_certificate = true
    }

    # client_key_config {
    #   issue_client_key = true
    # }
  }
}
# ... other resources for networking, firewall rules, etc.

# resource "google_dns_managed_zone" "app_zone" {
#   name        = "smitha-test"
#   dns_name    = "test@myapp.com."
#   description = "Managed Zone for myapp.com"
#}

#resource "google_dns_record_set" "app_dns" {
#   name    = "your-app"
#   zone    = google_dns_managed_zone.app_zone.name
#   type    = "A"
#   ttl     = "300"
#   rrdatas = [google_container_cluster.my_cluster.endpoint]
#}
